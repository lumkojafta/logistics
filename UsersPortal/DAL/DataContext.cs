﻿using Microsoft.EntityFrameworkCore;
using UsersPortal.Models;

namespace UsersPortal.DAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}
