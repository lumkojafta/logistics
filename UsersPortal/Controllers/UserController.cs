﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersPortal.DAL;
using UsersPortal.DTO;
using UsersPortal.Models;

namespace UsersPortal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly DataContext _repository;
        public UserController(DataContext repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<User>> GetUsers()
        {
            var users = GetAll();
            return Ok(users);
        }

        [HttpGet("Id")]
        public ActionResult<IEnumerable<User>> GetUsers(int id)
        {
            var users = GetAll().Where(x=> x.Id==id);
            return Ok(users);
        }

        [HttpDelete("Id")]
        public ActionResult<IEnumerable<User>> Delete(int id)
        {
            var users = GetAll().Where(x => x.Id == id);
            _repository.Remove(users);
            return Ok();
        }

        [HttpPost]
        public ActionResult Post([FromBody] UserDto value)     
        {

            var usserDB = new User();
            if (ModelState.IsValid)
            {
                usserDB = new User
                {
                    Id = value.Id,
                    FirstName = value.FirstName,
                    LastName = value.LastName,
                    Email = value.Email,
                    DateOfBirth = value.DateOfBirth
                };
                _repository.Users.Add(usserDB);
                _repository.SaveChangesAsync();
            }
            else
            {
                return BadRequest(ModelState);
            }
            return Ok(usserDB);
        }

        [HttpPut("UpdatUser/{id}")]
        public async Task<IActionResult> UpdateCustomerOrderStatus(int id, [FromBody] UserDto value)
        {
            var fromDB = _repository.Users.FirstOrDefault(x => x.Id == id);
            if (fromDB == null)
                return NotFound();            
            _repository.Users.Update(fromDB);
            await _repository.SaveChangesAsync();
            return Ok(fromDB);
        }

        private IQueryable<User> GetAll()
            {
            return _repository.Users.AsQueryable();
            }
    }
}
